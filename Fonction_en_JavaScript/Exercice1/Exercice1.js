/*
Auteur : Trehoux Simon
Date : 27/04/2021
*/
console.log("Calcul du temps de parcours d'un trajet :");
//init des valeurs
let vitesse = 90; //vitesse en km/h
let distance = 500; //distance en km
let temps = 0; //temp en seconde
let tempsh = ""; //temps heure minutes seconde
//let tempsH = 0;
console.log("   Vitesse moyenne (en km/h) : " + vitesse); // affichage de la vitesse
console.log("   Distance à parcourir (en km) : " + distance); //affichage e la distance

function calculerTempsParcoursSec(prmVitesse, prmDistance) {
  //creation de fonction calculerTempsParcoursSec avec les parametre vitesse et distance
  let tempsParcours = 0; // creation variable tempsParcours init a 0
  //calcul temps parcouru en seconde
  tempsParcours = prmDistance / prmVitesse;
  tempsParcours = tempsParcours * 3600;
  return tempsParcours; // retourne la valeur temps parcours
}

temps = calculerTempsParcoursSec(vitesse, distance); //appel de fonction de temps qui prend la valeurs retorune

function convertisseur(prmTemps) {
  //creation fonction convertisseur avec le parametre temps
  //variables qui stockeront les heures les minutes et les secondes
  let heure;
  let minute;
  let seconde;
  //creation chaine de caractere
  let affichage;

  heure = prmTemps / 3600; //calcul des heures
  minute = (prmTemps % 3600) / 60; //calcul des minutes restantes
  seconde = (prmTemps % 3600) % 60; //calcul des secondes restantes
  affichage =
    Math.floor(heure) +
    "H  " +
    Math.floor(minute) +
    "mm  " +
    Math.floor(seconde); //ajout des valeurs a la chaine de caractere
  return affichage; //retourne affichage
}

tempsh = convertisseur(temps); //appel de fonction de tempsh qui prend la valeur retourne

console.log(
  "A " +
    vitesse +
    " km/h, une distance de " +
    distance +
    " km est parcourue en " +
    tempsh +
    "s"
); //affichage final dans la console
