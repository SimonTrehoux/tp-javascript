/*
Auteur : Trehoux Simon
Date : 27/04/2021
*/
console.log("Recherche des multiples de 3 :");
let max = 20; //init du  la limite
console.log("Valeur limite de la recherche : " + max);
let affichage = ""; //init de la chaine caract generale

function rechercher_Mult3(prmMax) {
  // init fonction  rechercher_Mult3
  let chaine = "0"; // init chaine caract fonction
  for (let i = 1; i < prmMax + 1; i++) {
    // pour i llant de 1 à prmmax+1 avec un pas de 1
    let test; //init de la valeur test
    test = i % 3; //test de multiple de 3
    if (test == 0) {
      // si multiple de 3
      chaine = chaine + "-" + i; // ajouter la valeur a la chaine
    }
  }
  return chaine; //retourner la chaine
}
affichage = rechercher_Mult3(max); //appel de fonction , affichage prend la valeur de chaine

console.log("Multiples de 3 : " + affichage); //affichage des multiple possible dans la fourchette
