# TP JavaScript
Mes TP en JavaScript
> - Auteur : SIMON TREHOUX
> - Date de publication : 27/04/2021

## SOMMAIRE
## Introduction
- [HelloWorld](HelloWorld_js/index.html)
## Fonction en JavaScript
- [Exercice1](Exercice1/readme.md)
- [Exercice2](Exercice2/readme.md)