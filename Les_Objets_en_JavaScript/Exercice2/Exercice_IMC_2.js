/*
Auteur : Trehoux Simon
Date : 04/05/2021
*/

function patient(prmNom, prmPrenom, prmSexe, prmAge, prmTaille, prmPoids) {
                                            //Création de la fonction pour pouvoir decrire
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.sexe = prmSexe;
    this.age = prmAge;
    this.taille = prmTaille;
    this.poids = prmPoids;
    this.decrire = function () {
      let description;
      description =
        "Le patient" +
        this.prenom +
        " " +
        this.nom +
        " de sexe " +
        this.sexe +
        " est agé de " +
        this.age +
        " ans , fait " +
        this.taille +
        "cm et fait " +
        this.poids +
        "kg";
      return description;
    },

      this.decrire_corpulence = function () {
        let poids = this.poids;
        let taille = this.taille;
        let sexe = this.sexe;
        let imc = 0;
        let definition_corpu;
  
        function calculer_IMC() {
                                            //creation de la fonction calculer_ImC
          imc = poids / ((taille / 100) * (taille / 100)); //calcul de L'IMC
          imc = imc.toFixed(2);
          return imc;
        }
  
        function Interpreter_IMC() {
                                            //creation de la fonction Interpreter_IMC
  
          let affichage = " Il/Elle est en situation de ";     //creation de la chaine de caractere affichage
          if (sexe == "feminin") {
            if (imc < 16.5) {

              //si IMC est inferieur à 16.5
              affichage = affichage + "Dénutrition";
            } else if (imc < 18.5) {

              //si IMC est inferieur à 18.5
              affichage = affichage + "Maigreur";
            } else if (imc < 25) {

              //si IMC est inferieur à 25
              affichage = affichage + "Corpulence normale";
            } else if (imc < 30) {

              //si IMC est inferieur à 30
              affichage = affichage + "Surpoids";
            } else if (imc < 35) {

              //si IMC est inferieur à 35
              affichage = affichage + "Obesité modérée";
            } else if (imc < 40) {
              //si IMC est inferieur à 40
              affichage = affichage + "Obesité sévère";
            } else if (imc > 40) {

              //si IMC est superieur à 40
              affichage = affichage + "Obésité morbide";
            }
            return affichage;
          } else {
            if (imc < 16.5) {

              //si IMC est inferieur à 16.5
              affichage = affichage + "Dénutrition";
            } else if (imc < 18.5) {

              //si IMC est inferieur à 18.5
              affichage = affichage + "Maigreur";
            } else if (imc < 25) {

              //si IMC est inferieur à 25
              affichage = affichage + "Corpulence normale";
            } else if (imc < 30) {

              //si IMC est inferieur à 30
              affichage = affichage + "Surpoids";
            } else if (imc < 35) {

              //si IMC est inferieur à 35
              affichage = affichage + "Obesité modérée";
            } else if (imc < 40) {

              //si IMC est inferieur à 40
              affichage = affichage + "Obesité sévère";
            } else if (imc > 40) {
                
              //si IMC est superieur à 40
              affichage = affichage + "Obésité morbide";
            }
          }
          return affichage;
        }
  
        let imc_fin = calculer_IMC();
  
        definition_corpu = "Son IMC est de : " + imc_fin + Interpreter_IMC(imc);
  
        return definition_corpu;
      };
  }
  
  //creation des parametre pour objPatient
  let objPatient1 = new patient("Dupond", "Jean", "masculin", 30, 180, 85);
  let objPatient2 = new patient("Moulin", "Isabelle", "feminin", 46, 158, 74);
  let objPatient3 = new patient("Martin", "Eric", "masculin", 42, 165, 90);
  
  console.log(objPatient1.decrire()); //afficher le resultat la fonction decrire de objPatient
  console.log(objPatient1.decrire_corpulence()); //afficher l'IMC obtenue et son interpretation
  console.log(objPatient2.decrire()); //afficher le resultat la fonction decrire de objPatient
  console.log(objPatient2.decrire_corpulence()); //afficher l'IMC obtenue et son interpretation
  console.log(objPatient3.decrire()); //afficher le resultat la fonction decrire de objPatient
  console.log(objPatient3.decrire_corpulence()); //afficher l'IMC obtenue et son interpretation
  
  //Fin du code