/*
Auteur : Trehoux Simon
Date : 04/05/2021
*/
let objPatient = {                      //Création des parametre pour l'objet Patient
    nom : "Dupont",
    prenom : "Jean",
    age : 30,
    sexe : "masculin",
    taille : 180,
    poids : 85,

    decrire: function () {              //Création de la fonction pour pouvoir decrire
        let description;
        description =
          "Le patient" +
          this.prenom +
          " " +
          this.nom +
          " de sexe " +
          this.sexe +
          " est agé de " +
          this.age +
          " ans , fait " +
          this.taille +
          "cm et fait " +
          this.poids +
          "kg";
        return description;
      },

      calculer_IMC: function () {     //Creation de la fonction pour permettre de pouvoir calculer_ImC
        let IMCVALEUR;
        IMCVALEUR = this.poids / ((this.taille / 100) * (this.taille / 100)); //calcul de L'IMC
        IMCVALEUR = IMCVALEUR.toFixed(2);
        return IMCVALEUR;
      },

      Interpreter_IMC: function (prmIMC) {          //creation de la fonction Interpreter_IMC
        let affichage = "";                         //creation de la chaine de caractere affichage
        if (prmIMC < 16.5) {
          
            //si IMC est inferieur à 16.5
          affichage = affichage + "Dénutrition";
        } else if (prmIMC < 18.5) {
          
            //si IMC est inferieur à 18.5
          affichage = affichage + "Maigreur";
        } else if (prmIMC < 25) {
          
            //si IMC est inferieur à 25
          affichage = affichage + "Corpulence normale";
        } else if (prmIMC < 30) {
          
            //si IMC est inferieur à 30
          affichage = affichage + "Surpoids";
        } else if (prmIMC < 35) {
          
            //si IMC est inferieur à 35
          affichage = affichage + "Obesité modérée";
        } else if (prmIMC < 40) {
          
            //si IMC est inferieur à 40
          affichage = affichage + "Obesité sévère";
        } else if (prmIMC > 40) {
          
            //si IMC est superieur à 40
          affichage = affichage + "Obésité morbide";
        }
        return affichage;
      },
    };

    let imc = objPatient.calculer_IMC(); //appel de la fonction calculer par imc qui en prend la valeur

console.log(objPatient.decrire()); //afficher le resultat la fonction decrire de objPatient
console.log("Son IMC est de :" + imc); //afficher l'IMC obtenue dans la variable imc
console.log("Il est en situation de " + objPatient.Interpreter_IMC(imc) + "."); //afficher le resultat la fonction Interpreter_IMC de objPatient

//Fin du code 
    
    